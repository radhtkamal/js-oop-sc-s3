// by convention, class names should begin with an uppercase character
class Student {
	// the constructor method defines how objects created from this class will be assigned their initial propery values
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passingStatus = undefined;
		this.honorStatus = undefined;

		if (grades.length ===4) {
			if (grades.every(grade => grade >=0 && grade<= 100)) {
				this.grades = grades;
			} else {
				this.grades = undefined;
			}
		}
	}

	login() {
		console.log(`${this.email} has logged in.`);
		return this;
	}

	logout() {
		console.log(`${this.email} has logged out.`);
		return this;
	}

	listGrades() {
		console.log(`${this.name}'s quarterly averages are: ${this.grades}`);
		return this;
	}

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4;
		return this;
	}

	willPass() {
		if (this.gradeAve >= 85) {
			this.passingStatus = true;
		} else {
			this.passingStatus = false;
		}
		return this;
	}

	willPassWithHonors(){
		if(this.gradeAve >= 90){
			this.honorStatus = true;
		}else if(this.gradeAve < 90 && this.gradeAve >= 85){
			this.honorStatus = false;
		}else if(this.gradeAve < 85){
			this.honorStatus = false;
		}
		return this;
	}
}

// instantiate objects from our student class
let studentOne = new Student('John', 'john@mail.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@mail.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@mail.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

console.log(studentOne);
console.log(studentTwo);
console.log(studentThree);
console.log(studentFour)